
---
title: Start your Journey
subtitle: Start your Journey
comments: false
---

At MyCollegeDreams, we strongly believe that our vast experience and in-depth knowledge of the colleges, financial aid, scholarships and college admission processes are inimitable values to the services we offer both the student and their families. 

Start your journey by completing an initial self-assessment. Download the student [self-assessment form](/files/mcd-self-assessment.pdf) and fill in all the details and email it to us to get started with a FREE consultation.   
